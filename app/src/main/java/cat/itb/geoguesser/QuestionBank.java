package cat.itb.geoguesser;

import java.util.ArrayList;

public class QuestionBank {

    public static String getQuestion() {
        return "Which country does this flag represent?";
    }

    public static ArrayList<String[]> getFlagOptions() {
        ArrayList<String[]> flagOptions = new ArrayList<>();
        String[] question1to4 = new String[]{"Austria", "China", "Denmark", "Vietnam"};
        String[] question5to8 = new String[]{"Australia", "England", "Malaysia", "U.S.A"};
        String[] question9to12 = new String[]{"Bahamas", "Botswana", "Mozambique", "South Africa"};
        String[] question13to16 = new String[]{"Canada", "France", "Germany", "Greece"};
        String[] question17to20 = new String[]{"Ghana", "India", "Iran", "Nepal"};
        String[] question21to24 = new String[]{"Bangladesh", "Brasil", "Nigeria", "New Zealand"};

        for (int i = 0; i < 4; i++) {
            flagOptions.add(question1to4);
        }
        for (int i = 0; i < 4; i++) {
            flagOptions.add(question5to8);
        }
        for (int i = 0; i < 4; i++) {
            flagOptions.add(question9to12);
        }
        for (int i = 0; i < 4; i++) {
            flagOptions.add(question13to16);
        }
        for (int i = 0; i < 4; i++) {
            flagOptions.add(question17to20);
        }
        for (int i = 0; i < 4; i++) {
            flagOptions.add(question21to24);
        }
        return flagOptions;
    }

    public static ArrayList<Integer> getFlagAnswers() {
        ArrayList<Integer> flagAnswers = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            flagAnswers.add(1);
            flagAnswers.add(2);
            flagAnswers.add(3);
            flagAnswers.add(4);
        }
        return flagAnswers;
    }

    public static ArrayList<Integer> getFlagHints() {
        ArrayList<Integer> flagHints = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            flagHints.add(1);
            flagHints.add(2);
            flagHints.add(3);
            flagHints.add(4);
        }
        return flagHints;
    }

}
