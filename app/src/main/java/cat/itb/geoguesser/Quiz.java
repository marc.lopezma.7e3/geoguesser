package cat.itb.geoguesser;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.Random;

import static java.lang.Thread.sleep;

public class Quiz extends AppCompatActivity {

    static final ArrayList<QuestionModel> flagsBank = new ArrayList<>();
    static final ArrayList<QuestionModel> monumentsBank = new ArrayList<>();
    static final ArrayList<QuestionModel> foodBank = new ArrayList<>();
    ProgressBar progressBar;
    CountDownTimer countDownTimer;
    TextView questionNumber;
    TextView scoreTextView;
    TextView hintTextView;
    ImageButton hintButton;
    ImageView questionImage;
    RelativeLayout optionButtons;
    Button aButton;
    Button bButton;
    Button cButton;
    Button dButton;
    TextView questionText;
    private int barProgress = 0;
    private int maxTime = 5000;
    private int updateInverval = 100;
    private int hints;
    private double score = 0;
    private int questionProgress = 0;
    private ArrayList<QuestionModel> bank;

    private static void fillFlagsBank() {
        flagsBank.add(new QuestionModel(R.drawable.austria, getFlagsQuestionText(), new String[]{"Austria", "China", "Denmark", "Vietnam"}, "Austria", 'a'));
        flagsBank.add(new QuestionModel(R.drawable.china, getFlagsQuestionText(), new String[]{"Austria", "China", "Denmark", "Vietnam"}, "China", 'b'));
        flagsBank.add(new QuestionModel(R.drawable.denmark, getFlagsQuestionText(), new String[]{"Austria", "China", "Denmark", "Vietnam"}, "Denmark", 'c'));
        flagsBank.add(new QuestionModel(R.drawable.vietnam, getFlagsQuestionText(), new String[]{"Austria", "China", "Denmark", "Vietnam"}, "Vietnam", 'd'));

        flagsBank.add(new QuestionModel(R.drawable.australia, getFlagsQuestionText(), new String[]{"Australia", "England", "Malaysia", "USA"}, "Australia", 'a'));
        flagsBank.add(new QuestionModel(R.drawable.england, getFlagsQuestionText(), new String[]{"Australia", "England", "Malaysia", "USA"}, "England", 'b'));
        flagsBank.add(new QuestionModel(R.drawable.malaysia, getFlagsQuestionText(), new String[]{"Australia", "England", "Malaysia", "USA"}, "Malaysia", 'c'));
        flagsBank.add(new QuestionModel(R.drawable.usa, getFlagsQuestionText(), new String[]{"Australia", "England", "Malaysia", "USA"}, "USA", 'd'));

        flagsBank.add(new QuestionModel(R.drawable.bahamas, getFlagsQuestionText(), new String[]{"Bahamas", "Botswana", "Mozambique", "South Africa"}, "Bahamas", 'a'));
        flagsBank.add(new QuestionModel(R.drawable.botswana, getFlagsQuestionText(), new String[]{"Bahamas", "Botswana", "Mozambique", "South Africa"}, "Botswana", 'b'));
        flagsBank.add(new QuestionModel(R.drawable.mozambique, getFlagsQuestionText(), new String[]{"Bahamas", "Botswana", "Mozambique", "South Africa"}, "Mozambique", 'c'));
        flagsBank.add(new QuestionModel(R.drawable.south_africa, getFlagsQuestionText(), new String[]{"Bahamas", "Botswana", "Mozambique", "South Africa"}, "South Africa", 'd'));

        flagsBank.add(new QuestionModel(R.drawable.canada, getFlagsQuestionText(), new String[]{"Canada", "France", "Germany", "Greece"}, "Canada", 'a'));
        flagsBank.add(new QuestionModel(R.drawable.france, getFlagsQuestionText(), new String[]{"Canada", "France", "Germany", "Greece"}, "France", 'b'));
        flagsBank.add(new QuestionModel(R.drawable.germany, getFlagsQuestionText(), new String[]{"Canada", "France", "Germany", "Greece"}, "Germany", 'c'));
        flagsBank.add(new QuestionModel(R.drawable.greece, getFlagsQuestionText(), new String[]{"Canada", "France", "Germany", "Greece"}, "Greece", 'd'));

        flagsBank.add(new QuestionModel(R.drawable.ghana, getFlagsQuestionText(), new String[]{"Ghana", "India", "Iran", "Nepal"}, "Ghana", 'a'));
        flagsBank.add(new QuestionModel(R.drawable.india, getFlagsQuestionText(), new String[]{"Ghana", "India", "Iran", "Nepal"}, "India", 'b'));
        flagsBank.add(new QuestionModel(R.drawable.iran, getFlagsQuestionText(), new String[]{"Ghana", "India", "Iran", "Nepal"}, "Iran", 'c'));
        flagsBank.add(new QuestionModel(R.drawable.nepal, getFlagsQuestionText(), new String[]{"Ghana", "India", "Iran", "Nepal"}, "Nepal", 'd'));

        flagsBank.add(new QuestionModel(R.drawable.bangladesh, getFlagsQuestionText(), new String[]{"Bangladesh", "Brasil", "Nigeria", "New Zealand"}, "Bangladesh", 'a'));
        flagsBank.add(new QuestionModel(R.drawable.brazil, getFlagsQuestionText(), new String[]{"Bangladesh", "Brasil", "Nigeria", "New Zealand"}, "Brasil", 'b'));
        flagsBank.add(new QuestionModel(R.drawable.nigeria, getFlagsQuestionText(), new String[]{"Bangladesh", "Brasil", "Nigeria", "New Zealand"}, "Nigeria", 'c'));
        flagsBank.add(new QuestionModel(R.drawable.new_zealand, getFlagsQuestionText(), new String[]{"Bangladesh", "Brasil", "Nigeria", "New Zealand"}, "New Zealand", 'd'));
    }

    private static void fillMonumentsBank() {
        //INTRODUIR DIFERENTS PREGUNTES
    }

    private static void fillFoodBank() {
        //INTRODUIR DIFERENTS PREGUNTES
    }

    private static int getRandomNumber(String gameMode) {
        int length = 0;
        switch (gameMode) {
            case "flags":
                length = flagsBank.size();
                break;
            case "monuments":
                length = monumentsBank.size();
                break;
            case "food":
                length = foodBank.size();
                break;
            default:
                System.out.println("FATAL ERROR");
                break;
        }
        Random r = new Random();
        return r.nextInt(length);
    }

    public static ArrayList<QuestionModel> getQuestions(int num, String gameMode) {
        ArrayList<QuestionModel> bank = new ArrayList<>();
        switch (gameMode) {
            case "flags":
                fillFlagsBank();
                for (int i = 0; i < num; i++) {
                    QuestionModel newQuestion = flagsBank.get(getRandomNumber(gameMode));
                    if (!bank.contains(newQuestion)) {
                        bank.add(newQuestion);
                    } else {
                        i--;
                    }
                }
                break;
            case "monuments":
                //fillMonumentsBank();
                for (int i = 0; i < 10; i++) {
                    QuestionModel newQuestion = monumentsBank.get(getRandomNumber(gameMode));
                    if (!bank.contains(newQuestion)) {
                        bank.add(newQuestion);
                    } else {
                        i--;
                    }
                }
                break;
            case "food":
                //fillFoodBank();
                for (int i = 0; i < 10; i++) {
                    QuestionModel newQuestion = foodBank.get(getRandomNumber(gameMode));
                    if (!bank.contains(newQuestion)) {
                        bank.add(newQuestion);
                    } else {
                        i--;
                    }
                }
                break;
            default:
                System.out.println("FATAL ERROR");
        }

        return bank;
    }

    public static String getFlagsQuestionText() {
        return "What country does this flag represent?";
    }

    public static String getMonumentsQuestionText() {
        return "Where is this landmark located?";
    }

    public static String getFoodQuestionText() {
        return "Where's this food from?";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_layout);

        String gameMode = getIntent().getStringExtra("gameMode");

        progressBar = findViewById(R.id.progressBar);
        questionNumber = findViewById(R.id.questionNumber);
        scoreTextView = findViewById(R.id.scoreTextView);
        hintTextView = findViewById(R.id.hintTextView);
        hintButton = findViewById(R.id.hintButton);
        questionImage = findViewById(R.id.questionImage);
        questionText = findViewById(R.id.questionText);
        progressBar = findViewById(R.id.progressBar);
        optionButtons = findViewById(R.id.optionButtons);
        aButton = findViewById(R.id.aButton);
        bButton = findViewById(R.id.bButton);
        cButton = findViewById(R.id.cButton);
        dButton = findViewById(R.id.dButton);
        hintButton.setImageResource(R.drawable.hint_icon);

        if (savedInstanceState != null) {
            int questionProgress = savedInstanceState.getInt("questionProgress");
            Double score = savedInstanceState.getDouble("score");
            int hints = savedInstanceState.getInt("hints");
            continuar(gameMode, questionProgress, score, hints);
        } else {
            iniciar(gameMode);
        }
        aButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopProgress();
                comprovarResposta('a');
            }
        });

        bButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopProgress();
                comprovarResposta('b');
            }
        });

        cButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopProgress();
                comprovarResposta('c');
            }
        });

        dButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopProgress();
                comprovarResposta('d');
            }
        });

        hintButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopProgress();
                if (hints > 0) {
                    hints -= 1;
                    Toast.makeText(Quiz.this, bank.get(questionProgress).hint, Toast.LENGTH_SHORT).show();
                    hintTextView.setText(String.valueOf(hints));
                    refresh();
                    if (hints == 0) {
                        Toast.makeText(Quiz.this, "NO HINTS LEFT!", Toast.LENGTH_SHORT).show();
                        hintButton.setVisibility(View.INVISIBLE);
                        hintTextView.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("questionProgress", questionProgress);
        outState.putDouble("score", score);
        outState.putInt("hints", hints);
    }

    private void refresh() {
        setQuestionColors();
        questionProgress++;
        if (questionProgress == bank.size()) {
            stopProgress();
            actualitzarScore();
            AlertDialog.Builder builder = new AlertDialog.Builder(Quiz.this);
            builder.setTitle("Job's DONE!");
            builder.setMessage("You have successfully finished the QUIZ with a total score of: " + "\n" + (int) score * 10 + "/100" + "\n\n" + "Press NAY to quit or YAY to play again.");
            builder.setPositiveButton("Yay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent goBack = new Intent(Quiz.this, MainActivity.class);
                    startActivity(goBack);
                }
            });
            builder.setNegativeButton("Nay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        ActivityCompat.finishAffinity(Quiz.this); //with v4 support library
                        finishAffinity();
                        System.exit(0);
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                }
            });
            builder.create();
            builder.show();
        } else {
            actualitzarPregunta(questionProgress);
        }
    }

    private void setQuestionColors() {
        aButton.setTextColor(getResources().getColor(R.color.black));
        bButton.setTextColor(getResources().getColor(R.color.black));
        cButton.setTextColor(getResources().getColor(R.color.black));
        dButton.setTextColor(getResources().getColor(R.color.black));
    }

    private void iniciar(String gameModeString) {
        hints = 3;
        bank = getQuestions(10, gameModeString);
        hintTextView.setText(String.valueOf(hints));
        actualitzarPregunta(questionProgress);
    }

    private void continuar(String gameModeString, int questionProgress, double previousScore, int hintsLeft) {
        hints = hintsLeft;
        bank = getQuestions(10 - questionProgress, gameModeString);
        hintTextView.setText(String.valueOf(hintsLeft));
        score = previousScore;
        scoreTextView.setText(String.valueOf(score));
        actualitzarPregunta(questionProgress);

    }

    private void comprovarResposta(char resposta) {
        highlightCorrectAnswer();
        if (resposta == bank.get(questionProgress).answer) {
            Toast.makeText(Quiz.this, "CORRECT", Toast.LENGTH_SHORT).show();
            score += 1;
        } else {
            Toast.makeText(Quiz.this, "INCORRECT", Toast.LENGTH_SHORT).show();
            score -= 0.5;
        }
        refresh();
    }

    private void highlightCorrectAnswer() {
        switch (bank.get(questionProgress).answer) {
            case 'a':
                aButton.setTextColor(getResources().getColor(R.color.right));
                bButton.setTextColor(getResources().getColor(R.color.wrong));
                cButton.setTextColor(getResources().getColor(R.color.wrong));
                dButton.setTextColor(getResources().getColor(R.color.wrong));
                break;
            case 'b':
                aButton.setTextColor(getResources().getColor(R.color.wrong));
                bButton.setTextColor(getResources().getColor(R.color.right));
                cButton.setTextColor(getResources().getColor(R.color.wrong));
                dButton.setTextColor(getResources().getColor(R.color.wrong));
                break;
            case 'c':
                aButton.setTextColor(getResources().getColor(R.color.wrong));
                bButton.setTextColor(getResources().getColor(R.color.wrong));
                cButton.setTextColor(getResources().getColor(R.color.right));
                dButton.setTextColor(getResources().getColor(R.color.wrong));
                break;
            case 'd':
                aButton.setTextColor(getResources().getColor(R.color.wrong));
                bButton.setTextColor(getResources().getColor(R.color.wrong));
                cButton.setTextColor(getResources().getColor(R.color.wrong));
                dButton.setTextColor(getResources().getColor(R.color.right));
                break;
        }
    }

    private void actualitzarPregunta(int progress) {
        updateProgress();
        questionImage.setImageResource(bank.get(progress).imageId);
        questionText.setText(bank.get(progress).questionText);
        aButton.setText(bank.get(progress).options[0]);
        bButton.setText(bank.get(progress).options[1]);
        cButton.setText(bank.get(progress).options[2]);
        dButton.setText(bank.get(progress).options[3]);
        actualitzarScore();
        actualitzarNumPregunta();
    }

    private void actualitzarScore() {
        scoreTextView.setText(String.valueOf(score));
    }

    private void actualitzarNumPregunta() {
        String questionNumberString = questionProgress + 1 + "/" + bank.size();
        questionNumber.setText(questionNumberString);
    }

    private void updateProgress() {
        progressBar.setProgress(barProgress);
        countDownTimer = new CountDownTimer(maxTime, updateInverval) {
            @Override
            public void onTick(long millisUntilFinished) {
                Log.v("Log_tag", "Tick of Progress" + barProgress + millisUntilFinished);
                barProgress++;
                progressBar.setProgress((int) barProgress * 100 / (maxTime / updateInverval));
            }

            @Override
            public void onFinish() {
                score -= 0.5;
                barProgress = 0;
                progressBar.setProgress(barProgress);
                refresh();
            }
        };
        countDownTimer.start();
    }

    private void stopProgress() {
        countDownTimer.cancel();
        barProgress = 0;
        progressBar.setProgress(barProgress);
    }

    private void sleepPls() {
        try {
            sleep(500);
        } catch (Exception e) {
        }

    }
}