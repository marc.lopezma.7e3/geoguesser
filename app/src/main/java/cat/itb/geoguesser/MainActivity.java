package cat.itb.geoguesser;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    TextView welcomeTextView;
    RadioGroup difficultyRadioGroup;
    RadioButton flagRadioButton;
    RadioButton monumentRadioButton;
    RadioButton hardRadioButon;
    Button startButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        welcomeTextView = findViewById(R.id.welcomeTextView);
        difficultyRadioGroup = findViewById(R.id.difficultyRadioGroup);
        flagRadioButton = findViewById(R.id.easyRadioButton);
        monumentRadioButton = findViewById(R.id.mediumRadioButton);
        hardRadioButon = findViewById(R.id.hardRadioButton);
        startButton = findViewById(R.id.startButton);

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent quiz = new Intent(MainActivity.this, Quiz.class);
                if (flagRadioButton.isChecked()) {
                    quiz.putExtra("gameMode", "flags");
                    startActivity(quiz);
                } else if (monumentRadioButton.isChecked()) {
                    quiz.putExtra("gameMode", "monuments");
                    startActivity(quiz);
                } else if (hardRadioButon.isChecked()) {
                    quiz.putExtra("gameMode", "food");
                    startActivity(quiz);
                } else {
                    Toast.makeText(MainActivity.this, "YOU MUST SET A DIFFICULTY!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}