package cat.itb.geoguesser;

public class QuestionModel {

    int imageId;
    String questionText;
    String[] options;
    String hint;
    Character answer;

    public QuestionModel(int imageId, String questionText, String[] options, String hint, Character answer) {
        this.imageId = imageId;
        this.questionText = questionText;
        this.options = options;
        this.hint = hint;
        this.answer = answer;
    }


}

